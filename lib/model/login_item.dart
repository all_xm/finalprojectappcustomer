class LoginItem {
  String createDate;
  String finishDate;
  String mainRecordImg;
  String processName;

  LoginItem(this.createDate, this.finishDate, this.mainRecordImg, this.processName);

  factory LoginItem.fromJson(Map<String, dynamic> json) {
    return LoginItem(
        json['createDate'],
        json['finishDate'],
        json['mainRecordImg'],
        json['processName']
    );
  }
}