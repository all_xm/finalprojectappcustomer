import 'package:app_special_production_erp/model/login_item.dart';

class LoginResponse {
  String contractDate;
  String contractNumber;
  String currentProgress;
  String customerName;
  bool isEndWork;
  String packageName;
  List<LoginItem> processStatus;

  LoginResponse(this.contractDate, this.contractNumber, this.currentProgress, this.customerName, this.isEndWork, this.packageName, this.processStatus);

  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    return LoginResponse(
        json['contractDate'],
        json['contractNumber'],
        json['currentProgress'],
        json['customerName'],
        json['isEndWork'],
        json['packageName'],
        json['processStatus'] == null ? [] : (json['processStatus'] as List).map((e) => LoginItem.fromJson(e)).toList()
    );
  }
}
