import 'login_response.dart';

class LoginResult {
  LoginResponse data;
  bool isSuccess;
  int code;
  String msg;

  LoginResult(this.data, this.isSuccess, this.code, this.msg);

  factory LoginResult.fromJson(Map<String, dynamic> json) {
    return LoginResult(
        LoginResponse.fromJson(json['data']),
        json['isSuccess'] as bool,
        json['code'],
        json['msg']
    );
  }
}