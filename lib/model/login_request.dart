class LoginRequest {
  String contractNumber;
  String customerPhone;
  String token;

  LoginRequest(this.contractNumber, this.customerPhone, this.token);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['contractNumber'] = this.contractNumber;
    data['customerPhone'] = this.customerPhone;
    data['token'] = this.token;

    return data;
  }
}
