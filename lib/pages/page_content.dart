import 'package:app_special_production_erp/model/login_item.dart';
import 'package:app_special_production_erp/model/login_response.dart';
import 'package:app_special_production_erp/permissions.dart';
import 'package:flutter/material.dart';

class PageContent extends StatefulWidget {
  final LoginResponse data;

  const PageContent(this.data, {Key? key}) : super(key: key);

  @override
  State<PageContent> createState() => _PageContentState();
}

class _PageContentState extends State<PageContent> {
  List<LoginItem> loginItem = [];

  void _getLoginItem() {
    setState(() {
      loginItem = widget.data.processStatus;
    });
  }

  @override
  void initState() {
    super.initState();
    _getLoginItem();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(height: 35),
          Container(
            padding: EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '계약자명 : ' + widget.data.customerName,
                  style: TextStyle(fontSize: 20),
                ),
                Text(
                  '계약번호 : ' + widget.data.contractNumber,
                  style: TextStyle(fontSize: 20),
                ),
                Text(
                  '계약일자 : ' + widget.data.contractDate,
                  style: TextStyle(fontSize: 20),
                ),
                Text(
                  '패키지명 : ' + widget.data.packageName,
                  style: TextStyle(fontSize: 20),
                ),
                Row(
                  children: [
                    Text('진행상황 : ', style: TextStyle(fontSize: 20)),
                    (widget.data.isEndWork) ? Text('완료', style: TextStyle(fontSize: 20)) : Text(widget.data.currentProgress, style: TextStyle(fontSize: 20)),
                  ],
                ),
                Permissions(),
              ],
            ),
          ),
          Container(
            height: 1,
            color: Colors.black,
            margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
          ),
          _list(),
        ],
      ),
    );
  }

  Widget _list() {
    if (loginItem == []) {
      return SizedBox(
        child: Text('진행 내역이 없습니다.'),
      );
    } else {
      return Expanded(
        child: ListView.builder(
          itemCount: loginItem.length,
          itemBuilder: (_, index) => Column(
            children: [
              Container(
                height: 180,
                color: Colors.black,
                child: Row(
                  children: [
                    SizedBox(width: 15),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                content: SingleChildScrollView(
                                  child: ListBody(
                                    children: <Widget>[
                                    Image(
                                    image: NetworkImage(loginItem[index].mainRecordImg),
                                    fit: BoxFit.cover,
                                  )],
                                  ),
                                ),
                              );
                            }
                          );
                        });
                      },
                      child: Image(
                        image: NetworkImage(loginItem[index].mainRecordImg),
                        width: 150,
                        height: 150,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 170,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            loginItem[index].processName,
                            style: TextStyle(color: Colors.white, fontSize: 20),
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            '시작일 : ' + loginItem[index].createDate,
                            style: TextStyle(color: Colors.white, fontSize: 20),
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            '종료일 : ' + loginItem[index].finishDate,
                            style: TextStyle(color: Colors.white, fontSize: 20),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
            ],
          ),
        ),
      );
    }
  }
}
